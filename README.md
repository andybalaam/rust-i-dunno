# I-DUNNO in Rust

This library implements the Internationalized Deliberately Unreadable Network
Notation (shortened as I-DUNNO) as defined in
[RFC 8771](https://www.rfc-editor.org/rfc/rfc8771.html). The library supports
encoding and decoding I-DUNNO representation, but the command line interface
only supports encoding and does not implement decoding, as the RFC recommends
the output of such a function SHOULD NOT be presented to humans.

The code and documentation borrow heavily from the
[Python implementation of I-DUNNO](https://pypi.org/project/i-dunno/) by Lily
Foster.

To get started using I-DUNNO immediately, see the
[I-DUNNO Creator](https://andybalaam.gitlab.io/web-i-dunno/) which is based
on this project.

## Install and use

First, [install Rust](https://www.rust-lang.org/tools/install).

Next, install `i-dunno`:

```bash
cargo install i-dunno
```

Finally, run it like this:

```bash
i-dunno 198.51.100.164
```

## Building

First, [install Rust](https://www.rust-lang.org/tools/install).

Now you may build with:

```bash
cargo test
```

Or, if you have GNU Make, try:

```bash
make
```

## Limitations

The current implementation is restricted to generating I-DUNNO encodings with
no padding bits.  Adding support for padding bits would be a good contribution
opportunity - see the notes in [combinations.rs](src/combinations.rs),
and Issue #1.

## More info

* [Announcing Rust I-DUNNO](https://www.artificialworlds.net/blog/2021/03/17/announcing-rust-i-dunno/)
* [Questions about RFC 8771](https://www.artificialworlds.net/blog/2021/03/17/questions-about-rfc-8771/)

## Additional tools

We recommend installing `cargo-watch` and `rustfmt`:

```bash
rustup component add rustfmt
cargo install cargo-watch
```

## Contributing

Contributions are welcome!  Make sure you only submit code you wrote, or that
you have permission to submit.

Feel free to log issues and suggest changes via merge requests.

You should definitely format your code with `cargo fmt` before submitting
merge requests.

## License and copyright

This project is copyright 2021 by Andy Balaam.

The design is inspired by the work of Lily Foster in
[Python I-DUNNO](https://pypi.org/project/i-dunno/).

Contains an adapted version of
[confusables_to_data.rs](https://github.com/PeterReid/unicode_skeleton/blob/master/data/confusables_to_data.rs)
by Peter Reid.

This code is released under the [MIT license](LICENSE).
