pub mod code_generation;
mod confusables;
mod idna_disallowed;

pub use confusables::{CONFUSABLES_CHARS, CONFUSABLES_STRS};
pub use idna_disallowed::idna_disallowed;
