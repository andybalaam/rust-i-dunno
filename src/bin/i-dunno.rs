use clap::{crate_version, App, Arg};
use std::io;

use i_dunno::data::code_generation::{process_confusables, process_idna};
use i_dunno::{encode, ConfusionLevel};

fn main() {
    let args = App::new("i-dunno")
        .version(crate_version!())
        .arg(
            Arg::with_name("process-idna")
                .help(
                    "Process downloaded idna information into Rust source code",
                )
                .long("process-idna"),
        )
        .arg(
            Arg::with_name("process-confusables")
                .help("Process confusables information into Rust source code")
                .long("process-confusables"),
        )
        .arg(
            Arg::with_name("IP_ADDRESS")
                .help("The IP address to process")
                .multiple(true),
        )
        .get_matches();

    if args.is_present("process-idna") {
        process_idna(io::stdin().lock(), io::stdout().lock());
    } else if args.is_present("process-confusables") {
        process_confusables(io::stdin().lock(), io::stdout().lock()).unwrap();
    } else if let Some(ips) = args.values_of("IP_ADDRESS") {
        for ip in ips {
            let encoded =
                encode(ip.parse().unwrap(), ConfusionLevel::Minimum).unwrap();
            println!("{}", encoded);
        }
    }
}
