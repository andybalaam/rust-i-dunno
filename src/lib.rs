mod bits;
mod bytesbits;
mod combinations;
mod confusion;
pub mod data;
mod decode;
mod encode;
mod i_dunno_unicode;
mod vecboolbits;

pub use confusion::{confusion_level, ConfusionLevel};
pub use decode::decode;
pub use encode::{encode, encode_all};
