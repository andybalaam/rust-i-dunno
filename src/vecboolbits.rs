use crate::bits::Bits;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct VecBoolBits {
    vec: Vec<bool>,
    idx: usize,
}

impl VecBoolBits {
    #[cfg(test)]
    pub fn new(bits: Vec<bool>) -> Self {
        VecBoolBits { vec: bits, idx: 0 }
    }
}

impl Iterator for VecBoolBits {
    type Item = bool;
    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.vec.get(self.idx).cloned();
        self.idx += 1;
        ret
    }
}

impl Bits for VecBoolBits {
    fn len(&self) -> usize {
        if self.vec.len() < self.idx {
            0
        } else {
            self.vec.len() - self.idx
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn stringify<B: Bits>(bits: B) -> String {
        let mut ret = String::new();
        for bit in bits {
            ret.push(if bit { '1' } else { '0' });
        }
        ret
    }

    fn bits2vecbool(s: &str) -> VecBoolBits {
        VecBoolBits::new(
            s.chars()
                .map(|c| if c == '1' { true } else { false })
                .collect(),
        )
    }

    #[test]
    fn empty_list_of_bytes_is_no_bits() {
        assert_eq!(stringify(bits2vecbool("")), "");
    }

    #[test]
    fn zero_bytes_is_zero_bits() {
        assert_eq!(stringify(bits2vecbool("000000")), "000000");
        assert_eq!(
            stringify(bits2vecbool("0000000000000000")),
            "0000000000000000"
        );
    }

    #[test]
    fn some_numbers_come_out_as_bits() {
        assert_eq!(
            stringify(bits2vecbool(
                "\
            11111111\
            10000000\
            00000110\
            00000000\
            ",
            )),
            "\
            11111111\
            10000000\
            00000110\
            00000000\
            ",
        );
    }
}
