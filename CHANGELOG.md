# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2021-03-30
### Added
- Link to the web-i-dunno project.

## [0.6.0] - 2021-03-25
### Added
- Decoding I-DUNNO into IPv4 and IPv6 addresses.
### Changed
- Allow limiting the number of encodings created by `encode_all` for IPv6
  addresses.

## [0.5.0] - 2021-03-17
### Added
- Display correct version number when asked.

## [0.0.2] - 2021-03-17
### Fixed
- Renamed executable from `main` to `i-dunno`.

## [0.0.1] - 2021-03-17
### Added
- Encoding of IPv4 and IPv6 addresses as I-DUNNO
