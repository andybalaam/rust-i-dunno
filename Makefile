all: test

test:
	cargo fmt
	cargo build
	cargo test

test-with-stdout:
	cargo fmt
	cargo build
	cargo test -- --nocapture

doc:
	cargo doc --open

watch:
	# Prerequisite: cargo install cargo-watch
	cargo watch -s 'clear; cargo test --color=always 2>&1 | head -40'

publish:
	cargo publish

target/data/IdnaMappingTable.txt:
	mkdir -p target/data
	curl \
		--location \
		--show-error \
		--silent \
		'https://www.unicode.org/Public/idna/latest/IdnaMappingTable.txt' \
		--output $@

process-idna: target/data/IdnaMappingTable.txt
	cargo run -- --process-idna \
		< target/data/IdnaMappingTable.txt \
		> src/data/idna_disallowed.rs.new
	mv src/data/idna_disallowed.rs.new src/data/idna_disallowed.rs

target/data/confusables.txt:
	mkdir -p target/data
	curl \
		--location \
		--show-error \
		--silent \
		'https://www.unicode.org/Public/security/latest/confusables.txt' \
		--output $@

process-confusables: target/data/confusables.txt
	cargo run -- --process-confusables \
		< $< \
		> src/data/confusables.rs.new
	mv src/data/confusables.rs.new src/data/confusables.rs
